<?php

namespace Database\Seeders;

use App\Models\WorktimeTracker;
use Illuminate\Database\Seeder;

class WorktimeTrackerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorktimeTracker::factory()->count(50)->create()->each(function(WorktimeTracker $worktimeTracker) {
            $worktimeTracker->save();
        });
    }
}
