<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WorktimeTrackerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->sentence,
            'user_id' => 1,
            'date' => $this->faker->date(),
            's_time'  => $this->faker->randomElement(["01:00:00", "05:00:00", "08:00:00", "09:00:00"]),
            'e_time'  => $this->faker->randomElement(["13:00:00", "15:00:00", "18:00:00", "19:00:00"]),
        ];
    }
}
