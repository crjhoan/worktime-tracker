<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WorktimeTracker extends Model
{
    use HasFactory;

    protected $with = ['user'];

    protected $appends = ['hours'];

    protected $dates = ['date'];

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->select("id", "name");
    }

    public function getHoursAttribute(): float|int
    {
        $firstTime=strtotime($this->s_time);
        $lastTime=strtotime($this->e_time);
        $timeDiff= $lastTime - $firstTime;
        return $timeDiff/3600;
    }
}
