<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWorktimeTrackerRequest;
use App\Http\Requests\UpdateWorktimeTrackerRequest;
use App\Models\WorktimeTracker;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class WorktimeTrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $trackers = WorktimeTracker::latest()->paginate(5);

        return view('worktime-tracker.index', compact('trackers'));
    }

    public function overview(): View
    {
        $trackers = WorktimeTracker::all();

        $monthly = $trackers
            ->groupBy(function (WorktimeTracker $item) {
                return $item->date->format('Y-m');
            })->map(function($rows) {
                return [
                    'hours' => $rows->sum('hours'),
                ];
            });

        $weekly = $trackers
            ->groupBy(function($item) {
                $created_at = Carbon::parse($item->date);
                $start = $created_at->startOfWeek()->format('d-m-Y');
                $end = $created_at->endOfWeek()->format('d-m-Y');

                return "{$start} - {$end}";
            })
            ->map(function($rows) {
                return [
                    'hours' => $rows->sum('hours'),
                ];
            });

        $daily = $trackers
            ->groupBy(function (WorktimeTracker $item) {
                return $item->date->format('Y-m-d');
            })->map(function($rows) {
                return [
                    'hours' => $rows->sum('hours'),
                ];
            });

        return view('worktime-tracker.overview', compact('monthly', 'weekly', 'daily'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('worktime-tracker.create');
    }

    /**
     * @param StoreWorktimeTrackerRequest $request
     * @return RedirectResponse
     */
    public function store(StoreWorktimeTrackerRequest $request): RedirectResponse
    {
        $date = $request->input("date");
        $worktimeTracker = new WorktimeTracker([
            'body' => $request->input("body"),
            'date' => $date,
            'user_id' => 1,
            "s_time" => $request->input("s-time"),
            "e_time" => $request->input("e-time"),
        ]);
        $worktimeTracker->save();

        return redirect()->route('worktime-trackers.index')->with('success', 'Entry created successfully.');
    }

    /**
     * @param WorktimeTracker $worktimeTracker
     * @return View
     */
    public function edit(WorktimeTracker $worktimeTracker): View
    {
        $tracker = $worktimeTracker;
        return view('worktime-tracker.edit', compact('tracker'));
    }

    /**
     * @param UpdateWorktimeTrackerRequest $request
     * @param WorktimeTracker $worktimeTracker
     * @return RedirectResponse
     */
    public function update(UpdateWorktimeTrackerRequest $request, WorktimeTracker $worktimeTracker): RedirectResponse
    {
        $date = $request->input("date");
        $worktimeTracker->update([
            'body' => $request->input("body", $worktimeTracker->body),
            'date' => $date,
            'user_id' => 1,
            "s_time" => $request->input("s-time"),
            "e_time" => $request->input("e-time"),
        ]);

        $worktimeTracker->refresh();
        return redirect()->route('worktime-trackers.index')
            ->with('success', 'Entry updated successfully');
    }

    /**
     * @param WorktimeTracker $worktimeTracker
     * @return RedirectResponse
     */
    public function destroy(WorktimeTracker $worktimeTracker): RedirectResponse
    {
        $worktimeTracker->delete();

        return redirect()->route('worktime-trackers.index')
            ->with('success', 'Entry deleted successfully');
    }

    /**
     * @param Request $request
     * @return StreamedResponse
     */
    public function exportCsv(Request $request): StreamedResponse
    {
        $fileName = 'worktime-tracker.csv';
        $trackers = WorktimeTracker::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Description', 'Date', 'Start Time', 'End Time', 'Hours');

        $callback = function() use($trackers, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($trackers as $tracker) {
                $row['Description']  = $tracker->body;
                $row['Date']    = $tracker->date;
                $row['Start Time']    = $tracker->s_time;
                $row['End Time']  = $tracker->e_time;
                $row['Hours']  = $tracker->hours;

                fputcsv($file, array($row['Description'], $row['Date'], $row['Start Time'], $row['End Time'], $row['Hours']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
