@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Worktime Tracker - OVERVIEW</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('worktime-trackers.index') }}" title="Go to index"> <i class="far fa-business-time"></i>
                    Entries
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
            <h1>Monthly</h1>
            <table class="table table-bordered table-responsive-lg">
                <tr>
                    <th>Month</th>
                    <th>Hours</th>
                </tr>
                @foreach ($monthly as $key => $item)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $item['hours'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <h1>Weekly</h1>
            <table class="table table-bordered table-responsive-lg">
                <tr>
                    <th>Week</th>
                    <th>Hours</th>
                </tr>
                @foreach ($weekly as $key => $item)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $item['hours'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <h1>Daily</h1>
            <table class="table table-bordered table-responsive-lg">
                <tr>
                    <th>Day</th>
                    <th>Hours</th>
                </tr>
                @foreach ($daily as $key => $item)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $item['hours'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
