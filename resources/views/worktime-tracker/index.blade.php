@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-success" href="{{ route('index') }}" title="Go to index"> <i class="far fa-business-time"></i>
                    Overview
                </a>
                <h2>Worktime Tracker </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('worktime-trackers.create') }}" title="Create a entry"> <i class="fas fa-plus-circle"></i>
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <a class="btn btn-success" href="{{ route('export') }}" title="Export"> <i class="far fa-business-time"></i>
        Export as CSV
    </a>
    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>ID</th>
            <th>Body</th>
            <th>User Name</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Hours</th>
            <th>Date Created</th>
            <th>Action</th>
        </tr>
        @foreach ($trackers as $tracker)
            <tr>
                <td>{{ $tracker->id }}</td>
                <td>{{ $tracker->body }}</td>
                <td>{{ $tracker->user->name }}</td>
                <td>{{ $tracker->date->format('Y-m-d') }}</td>
                <td>{{ $tracker->s_time }}</td>
                <td>{{ $tracker->e_time }}</td>
                <td>{{ $tracker->hours }}</td>
                <td>{{ date_format($tracker->created_at, 'jS M Y') }}</td>
                <td>
                    <form action="{{ route('worktime-trackers.destroy', $tracker->id) }}" method="POST">
                        <a href="{{ route('worktime-trackers.edit', $tracker->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $trackers->links() !!}

@endsection
